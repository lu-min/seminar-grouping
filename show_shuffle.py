import shuffle_grouping as sg

import tkinter
from tkinter import Tk, Scrollbar, Frame, Button
from tkinter.ttk import Treeview
from time import sleep
import threading

global stop
stop = False

students = sg.read_students()
solution = sg.assign_roles(sg.get_solution(students))
group_count = len(solution)

root = Tk()
root.geometry('840x320+400+300')
root.title('ランダムグルーピング')

frame = Frame(root)
frame.place(x=40, y=40, width=800, height=260)


def stopBtnClicked():
    global stop
    stop = not stop

btn = Button(root, text = 'ストップ！', command = stopBtnClicked)
btn.pack()

scrollBar = tkinter.Scrollbar(frame)
scrollBar.pack(side=tkinter.RIGHT, fill=tkinter.Y)

#Treeview
cols = []
# for i in range(group_count):
#     cols.append('c' + str(1 + 4 * i))
#     cols.append('c' + str(2 + 4 * i))
#     cols.append('c' + str(3 + 4 * i))
#     cols.append('c' + str(4 + 4 * i))
for i in range(9):
    cols.append('c' + str(i + 1))
tree = Treeview(frame, columns=tuple(cols), show="headings", yscrollcommand=scrollBar.set)

#setup columns
tree.column('c1', width=100, anchor='center')
tree.column('c2', width=60, anchor='center')
tree.column('c3', width=60, anchor='center')
tree.column('c4', width=120, anchor='center')
tree.column('c5', width=20, anchor='center')
tree.column('c6', width=100, anchor='center')
tree.column('c7', width=60, anchor='center')
tree.column('c8', width=60, anchor='center')
tree.column('c9', width=120, anchor='center')
tree.heading('c1', text='名前')
tree.heading('c2', text='学部')
tree.heading('c3', text='学科')
tree.heading('c4', text='役割分担')
tree.heading('c5', text='')
tree.heading('c6', text='名前')
tree.heading('c7', text='学部')
tree.heading('c8', text='学科')
tree.heading('c9', text='役割分担')

def show_data(solution):
    group_count = len(solution)
    for i in tree.get_children():
        tree.delete(i)
    row_count = 0
    grpIdx = 0
    while grpIdx < group_count:
        grp1 = solution[grpIdx]
        cnt = len(grp1)
        grp2 = None
        if grpIdx + 1 < group_count:
            grp2 = solution[grpIdx + 1]
            if len(grp2) > cnt:
                cnt = len(grp2)
        for i in range(cnt):
            vals =[]
            if grp1 and i < len(grp1):
                stu = grp1[i]
                vals.append(stu.name)
                vals.append(stu.department)
                vals.append(stu.major)
                vals.append(stu.role)
            else:
                vals.extend(['', '', '', ''])
            vals.append('|')
            if grp2 and i < len(grp2):
                stu = grp2[i]
                vals.append(stu.name)
                vals.append(stu.department)
                vals.append(stu.major)
                vals.append(stu.role)
            else:
                vals.extend(['', '', '', ''])
            tree.insert('', row_count, values = vals)
            row_count += 1
        hr = ['ーーーーーーーーーー' for i in range(9)]
        hr[4] = '＋'
        tree.insert('', row_count, values = hr)
        row_count += 1
        grpIdx += 2

show_data(solution)

tree.pack(side=tkinter.LEFT, fill=tkinter.Y)

#Treeview scroll bar
scrollBar.config(command=tree.yview)

def shuffle_loop():
    solution = sg.assign_roles(sg.get_solution(students))
    show_data(solution)
    if not stop:
        timer = threading.Timer(0.5, shuffle_loop)
        timer.start()
    else:
        with open('groups.csv', 'wt', encoding='utf-8-sig') as f:
            sg.output_groups(solution, f)

#treeview click event
def treeviewClick(event):
    pass

tree.bind('<Button-1>', treeviewClick)

shuffle_loop()

root.mainloop()

