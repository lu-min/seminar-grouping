settings = {
    'INPUT_PATH': 'data.csv',
    'SPLITTER': ',',
    'DATA_START_LINE': 0, # start from 0
    'DATA_COLUMNS': [0,1,2,3,4], # column index (start from 0) for 学生番号、名前、性別、学部、学科
    'FEMALE_STRING': '女性',
    'GROUP_NUMBER': 4,
    'OUTPUT_COUNT': 10,
    'OUTPUT_PATH': 'result.csv',
    'ROLES': ['最初の人', '声出し', 'ファシリテータ', '画面共有', '予備', '', '', '']
}

settings_presentation = {
    'INPUT_PATH': 'data.csv',
    'SPLITTER': ',',
    'DATA_START_LINE': 0, # start from 0
    'DATA_COLUMNS': [0,1,2,3,4], # column index (start from 0) for 学生番号、名前、性別、学部、学科
    'FEMALE_STRING': '女性',
    'GROUP_NUMBER': 3,
    'OUTPUT_COUNT': 10,
    'OUTPUT_PATH': 'result.csv',
    'ROLES': ['1', '2', '3', '4', '5', '6', '7', '8']
}