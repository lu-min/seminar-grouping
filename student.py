class Student(object):
    def __init__(self, data=[]):
        if len(data) > 0:
            self.student_id = data[0]
            self.name = data[1]
            self.gender = data[2]
            self.department = data[3]
            self.major = data[4]
            self.role = ""
    
    def __repr__(self):
        return repr((self.student_id, self.name, self.gender, self.department, self.major, self.role))