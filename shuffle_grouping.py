from student import Student
import config
import math as m
import random as rnd
settings = config.settings
STR_FEMALE = settings['FEMALE_STRING']
SPLITTER = settings['SPLITTER']
ROLES = settings['ROLES']

def read_from_csv(file_path, data_cols, start_line):
    res = []
    with open(file_path, encoding='utf-8-sig') as f:
        lines = f.readlines()
        for i in range(start_line, len(lines)):
            line = lines[i].strip()
            cells = line.split(',')
            data = []
            for col in data_cols:
                data.append(cells[col])
            res.append(Student(data))
    return res

def read_students():
    return read_from_csv(settings['INPUT_PATH'], settings['DATA_COLUMNS'], settings['DATA_START_LINE'])

def count_female(group):
    res = 0
    for stu in group:
        if stu.gender == STR_FEMALE:
            res += 1
    return res

def check_groups(groups):
    total_student = 0
    female_student = 0
    group_count = len(groups)
    for group in groups:
        total_student += len(group)
        female_student += count_female(group)
    min_stu = m.floor(total_student / group_count)
    max_stu = m.ceil(total_student / group_count)
    min_female = m.floor(female_student / group_count)
    max_female = m.ceil(female_student / group_count)
    min_male = m.floor((total_student - female_student) / group_count)
    max_male = m.ceil((total_student - female_student) / group_count)
    for group in groups:
        count = len(group)
        if count > max_stu:
            return False
        if count < min_stu:
            return False
        female_count = 0
        for stu in group:
            if stu.gender == STR_FEMALE:
                female_count += 1
        male_count = count - female_count
        if female_count > max_female:
            return False
        if female_count < min_female:
            return False
        if male_count > max_male:
            return False
        if male_count < min_male:
            return False
    return True

def group_by_major(students = []):
    sorted_stus = sorted(students, key=lambda student: student.major)
    res = []
    start = 0
    for i in range(len(sorted_stus) - 1):
        if sorted_stus[i].major != sorted_stus[i+1].major:
            group = sorted_stus[start:i+1]
            res.append(group)
            start = i + 1
    res.append(sorted_stus[start:-1])
    return res

def group_by_department(students = []):
    sorted_stus = sorted(students, key=lambda student: student.department)
    res = []
    start = 0
    for i in range(len(sorted_stus) - 1):
        if sorted_stus[i].department != sorted_stus[i+1].department:
            group = sorted_stus[start:i+1]
            res.append(group)
            start = i + 1
    res.append(sorted_stus[start:])
    return res

def get_available_groups(groups, max_stu):
    res = []
    for group in groups:
        if len(group) >= max_stu:
            continue
        res.append(group)
    return res

def shuffle_students(sorted_students, group_count, student_count):
    max_stu = m.ceil(student_count / group_count)
    res = []
    for i in range(group_count):
        res.append([])
    for department in sorted_students:
        stus = department.copy()
        while len(stus) > 0:
            groups = get_available_groups(res, max_stu)
            if len(stus) > len(groups):
                for group in groups:
                    idx = 0
                    if (len(stus) > 1):
                        idx = rnd.randint(0, len(stus)-1)
                    stu = stus[idx]
                    group.append(stu)
                    stus.remove(stu)
            else:
                idxs = rnd.sample(range(len(groups)), len(stus))
                for k in range(len(idxs)):
                    groups[idxs[k]].append(stus[k])
                stus.clear()
    return res

def output_groups(groups, f=None):
    count = 0
    lines = []
    for group in groups:
        count += 1
        line = 'Group ' + str(count)
        print(line)
        lines.append(line + '\n')
        for stu in group:
            print(stu.name + '\t' + stu.department + ' ' + stu.major + '\t' + stu.role + '\t' )
            line = stu.student_id + SPLITTER + stu.name + SPLITTER + stu.department + SPLITTER + stu.major + SPLITTER + stu.role
            lines.append(line + '\n')
    if f is not None:
        f.writelines(lines)
    return lines

def get_solutions(students, solution_count = 1, output_path = None):
    grouped = group_by_department(students)
    solutions = []
    for i in range(solution_count):
        flag = False
        while not flag:
            solution = shuffle_students(grouped, settings['GROUP_NUMBER'], len(students))
            flag = check_groups(solution)
            if flag:
                solutions.append(solution)
    if output_path is not None:
        with open(output_path, 'wt', encoding='utf-8-sig') as f:
            for i in range(solution_count):
                solution = solutions[i]
                line = 'Solution ' + str(i+1) + ':'
                print(line)
                f.write(line + '\n')
                output_groups(solution, f)
                f.write('\n')
                print('')

def get_solution(students):
    grouped = group_by_department(students)
    flag = False
    while not flag:
        solution = shuffle_students(grouped, settings['GROUP_NUMBER'], len(students))
        flag = check_groups(solution)
        if flag:
            return solution

def assign_roles(solution):
    # print(solution)
    for group in solution:
        roles = [(ROLES[i], rnd.random()) for i in range(len(group))]
        roles = sorted(roles, key=lambda x: x[1])
        for i in range(len(group)):
            group[i].role = roles[i][0]
    return solution

if __name__ == "__main__":
    students = read_from_csv(settings['INPUT_PATH'], settings['DATA_COLUMNS'], settings['DATA_START_LINE'])
    get_solutions(students, settings['OUTPUT_COUNT'], settings['OUTPUT_PATH'])
    # solution = assign_roles(get_solution(students))
    # print(solution)
